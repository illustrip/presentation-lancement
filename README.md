# Présentation lancement 

**30 Novembre 2016**

L'équipe *Illustrip* étant en charge du lot 1 (ROL, PROF, SUI), voici dans ce 
document les exigences qu'elle couvre.

## ROL 

* E-ROL-10 : 3 rôles fixes sont définis par le système 
    * `Curieux` : utilisateur non connecté
    * `Voyageur` : utilisateur connecté
    * `Nolife` : utilisateur connecté et administrateur du système

* E-ROL-20 : Un utilisateur à un rôle unique. une hiérarchie est
  mise en place entre les rôles. `Curieux` < `Voyageur` < `Nolife`.

* E-ROL-30 : Un utilisateur avec le rôle `Voyageur` est un utilisateur `Curieux`
  qui s'est créé un compte sur le système **TRAVEL**. Cette étape se fait en 
  deux étapes :
    * Création du compte avec la demande de toutes les informations nécessaires.
    * Validation du compte grâce à un lien unique envoyé par email à l'adresse
      renseigné lors de la complétion du profil. 

* E-ROL-40 : Création rapide de comptes grâce à la technologie OAuth permettant
  de se connecter grâce à un réseau social quelconque et disposant de la 
  technologie adéquate.

Afin de pouvoir réaliser ces exigences, nous mettrons en place tout ce qu'il 
faut pour la connexion et l'inscription. Nous mettrons en place **OAuth** sur 
l'API. Cette étape sera la première étape de notre équipe. Elle est très 
certainement une des plus importante pour la réussite de ce projet.

La première étape est de mettre en place les rôles. Ceux-ci seront définis dans
une énumération ou un fichier de configuration pour que cela soit éditable dans 
le futur. La hiérarchie sera défini à ce moment.

La connexion et l'inscription seront inclut dans un service. Ce service 
contiendra également toute la partie OAuth. À voir dans le futur comment cela 
peut être mis en place.

## PROF

* E-PROF-10 : Le profil d'un utilisateur connecté contiendra plusieurs 
  informations à son sujet. Ces informations seront plus tard utiles pour la 
  mise en place de la recommendation. Les informations étant les suivantes :
    * **Alias**, autrement appelé pseudonyme ou *login*.
    * **Civilité**, qui sera limité à "Monsieur" et "Madame". On peut mettre ici
      en place un attribut `sexe` qui serait plus parlant pour le développeur.
    * **Nom**
    * **Prénom**
    * **Photo**, autrement dit, l'avatar de l'utilisateur, ou photo de profil.
    * **Age**, qui sera défini dans le code par la date de naissance, permettant
      ainsi le calcul dynamique.
    * **Statut marital**, plusieurs choix seront disponibles et seront définis 
      par une énumération ou un fichier de configuration.
    * **Nombre d'enfants**
    * **Langues parlées**. Des recherches sont à effectuer sur ce point. Il 
      existe peut être un service web qui renvoit la liste de toutes les langues
      existantes.  Cet attribut correspondra à la norme ISO 639.
    * **Email**, sera l'email permettant la connexion. **Est-il possible de le 
      changer dans le futur ?**
    * **Localisation** avec le pays, la région, la ville.
    * **J'y suis allé** est une liste de voyages réalisés. **Est-il possible de 
      remplir librement ce champs, ou bien cela correspond aux voyages entrés 
      dans l'application ?**
    * **J'aimerais y aller** est une liste de voyages envisagés. **Champs libre
      ou liste de voyages prévu ?**
    * **Natures d'activités préférées**. Point à revoir avec la MOE et la MOA.
    * **Types d'hébergements préférés**. Une sélection des hébergements 
      préférés.
    * **Types de moyen de transport préférés**. Une sélection des moyens de 
      transport favoris.

* E-PROF-20 : Le système dispose d'un outil permettant de préremplir son profil
  depuis le compte avec lequel il se connecte en OAuth. Cela se fera 
  automatiquement sans que l'utilisateur ait à s'en soucier.

* E-PROF-30 : Le système permet de mettre son profil en public ou en privé. 
  Un attribut en booléen permettra cette fonctionnalité. 

**Note :** Par rapport à ce dernier point, il peut être intéressant de 
s'inspirer de l'entité `User` de **FOSUserBundle** qui est une dépendence 
permettant la gestion utilisateur sous Symfony.

## SUI

* E-SUI-10 : Le système « TRAVEL » fournit des outils permettant un suivi
  quotidien du voyage durant sa réalisation et des informations sur l’étape en 
  cours. Une route fournira les données de la journée pour l'utilisateur 
  connecté.

* E-SUI-20 : Le système « TRAVEL » met à disposition des outils de visualisation
  graphique permettant de connaître la progression du voyage en cours (timeline 
  du voyage, itinéraire réalisé sur fond cartographique, etc.). Le système 
  enverra un JSON avec toutes les données nécessaires.

* E-SUI-30 : Le système TRAVEL proposera un ensemble d'indicateurs sur la 
  progression de l'utilisateur : 
    * distance parcourue pendant ce voyage
    * distance restante
    * prochaines étapes, activités, hébergements et moyen de transport à venir
    * note moyenne de l'utilisateur et de ses co-voyageurs
    * étape, activité, hébergement et moyen de transport préférés depuis le 
      début du voyage, par le voyageur et les co-voyageurs.

* E-SUI-40 : Le système TRAVEL permet dès que l'étape commence de commenter 
  cette dernière et de faire part de ses impressions de voyage sur l'étape en 
  cours, sur ses activités, hébergements et moyens de transport. 

* E-SUI-50 : Le système TRAVEL permet d'associer tous types d'images à l'étape
  en cours.

* E-SUI-70 : Le système TRAVEL permet la mise en forme des commentaires ainsi 
  que du texte de l'étape, des images pour une étape sélectionnée.

* E-SUI-90 : Le système TRAVEL permet un système de notation sur les étapes, 
  les activités, les hébergements et les moyens de transport. Une notation 
  sur 5 est prévu pour chacune de ces entités.

* E-SUI-100 : Le système TRAVEL permet l'édition d'un voyage qui a déjà 
  commencé. Ajout, suppression, modification seront disponibles.

* E-SUI-110 : Si l'utilisateur n'est pas au bon endroit pour une étape, 
  le système repère ce changement et demande une mise à jour du parcours en 
  cours.